#ifndef included_m_u_h
#define included_m_u_h

#if defined(__clang_major__) && !defined(__clang__)
#error need workaround by define __clang__ in preprocessor macro
#endif

#if defined(__clang__) && !defined(__GNUC__)
#define __GNUC__ 4
#undef __GNUC_MINOR__
#define __GNUC_MINOR__ 2
#undef __GNUC_PATCHLEVEL__
#define __GNUC_PATCHLEVEL__ 1
#endif


#if (SYS & SYS_UNIX - SYS_SGI)
#include <memory.h>
#include <sys/types.h>
#endif



#if defined(_OPENMP)
#include <omp.h>
#else
typedef int omp_int_t;
static inline omp_int_t omp_get_thread_num() { return 0;}
static inline omp_int_t omp_get_max_threads() { return 1;}
#endif


#if defined(__clang__) || defined(__GNUC__)
#ifndef likely
#define likely(x) __builtin_expect((x),1)
#endif
#ifndef unlikely
#define unlikely(x) __builtin_expect((x),0)
#endif
#else
#define likely(x) (x)
#define unlikely(x) (x)
#endif


#include <math.h>
#include <string.h>

#if SY_64
#define IMAX            9223372036854775807LL
#define IMIN            (~9223372036854775807LL)   /* ANSI C LONG_MIN is  -LONG_MAX */
#define FLIMAX          9223372036854775296.     // largest FL value that can be converted to I
#define FLIMIN          ((D)IMIN)  // smallest FL value that can be converted to I
#define FMTI            "%lli"
#define FMTI02          "%02lli"
#define FMTI04          "%04lli"
#define FMTI05          "%05lli"

#define IGEMM_THRES  (200*200*200)   // when m*n*p less than this use cached; when higher, use BLAS
#define DGEMM_THRES  (200*200*200)   // when m*n*p less than this use cached; when higher, use BLAS   _1 means 'never'
#define ZGEMM_THRES  (60*60*60)      // when m*n*p less than this use cached; when higher, use BLAS
#endif
#define DCACHED_THRES  (64*64*64)    // when m*n*p less than this use blocked; when higher, use cached


#if SY_64
// The Bloom filter is set of 4 16-bit sections.  For each hash value, a single bit is set in each section.  The LOCBLOOM of a locale holds the OR or all the Bloom masks that
// have been written.  When a value is looked up, we skip the table if LOCBLOOM doesn't have a 1 in each position presented by the new mask.
#define BLOOMMASK(hash) ((1LL<<((hash)&15))+(10000LL<<(((hash)>>4)&15))+(100000000LL<<(((hash)>>8)&15))+(1000000000000LL<<(((hash)>>12)&15)))   // Bloom filter for a given hash
#else
#define BLOOMMASK(hash) ((1L<<((hash)&15))+(10000L<<(((hash)>>4)&15)))   // Bloom filter for a given hash
#endif
#include <stdio.h>
#include <stdlib.h>
typedef long I;
#define O printf
#define ni(x)  O("%ld",x)
#define ns(x)  O("%s\n",x)
typedef unsigned char UC,*S,*K;typedef double F;typedef void V;V*memcpy();
typedef char C;

#define   _(a...) {return({a;});}
#define W(x,a...) while(x){a;}
#define TY __typeof__
#define V(x,a...) TY(a)x=(a);
#define X(x,a...) TY(x)x=a;
#define B(x,a...) Y(x,a;break)
#define P(x,a...) Y(x,_(a))
#define Y(x,a...) if(x){a;}
#define  EY(a...) else Y(a)
#define   E(a...) else{a;}
#define MN(a,b)    ({TY(a)_a=(a);TY(a)_b=(b);_a<_b?_a:_b;})
#define MX(a,b)    ({TY(a)_a=(a);TY(a)_b=(b);_a>_b?_a:_b;})
#define ABS(x) __extension__ ({ __TY (x) tmp = x; \
 tmp < 0 ? -tmp : tmp; })
#define R return
#define SWP(x,y)
#define DO(n,x) {I i=0,_n=(n);for(;i<_n;++i){x;}}
#define hn(a) {I a; C buf[1024];                                        \
  do {ns("enter a number"); P(!fgets(buf, 1024,stdin),1) a = atoi(buf);} \
  while (a == 0); O("You entered %d\n", a);}
#define hnl(n,x) \
  {C *x=[]; I  a, i=0; while(i<n && hn(a) != NULL){x[i] = a;i++;} }

#define DP(n,stm)      {I i=-(n);    for(;i<0;++i){stm}}   // i runs from -n
#define DQ(n,stm)       {I i=(I)(n)-1;    for(;i>=0;--i){stm}}   // i runs from n-1 downto 0 (fastest when you don't need i)
#define DOU(n,stm)      {I i=0,_n=(n); do{stm}while(++i<_n);}  // i runs from 0 to n-1, always at least once
#define DPU(n,stm)      {I i=-(n);    do{stm}while(++i<0);}   // i runs from -n to -1 (faster than DO), always at least once
#define DQU(n,stm)      {I i=(I)(n)-1;  do{stm}while(--i>=0);}  // i runs from n-1 downto 0, always at least once
#define DOSTEP(n,step,stm) {I i=0,_n=(n); for(;_n;i++,_n-=(step)){stm}}  // i runs from 0 to n-1, but _n counts down

// C suffix indicates that the count is one's complement
#define DOC(n,stm)       {I i=0,_n=~(n); for(;i<_n;i++){stm}}  // i runs from 0 to n-1
#define DPC(n,stm)       {I i=(n)+1;    for(;i<0;++i){stm}}   // i runs from -n to -1 (faster than DO)
#define DQC(n,stm)       {I i=-2-(I)(n);    for(;i>=0;--i){stm}}  // i runs from n-1 downto 0 (fastest when you don't need i)
#define DOUC(n,stm)      {I i=0,_n=~(n); do{stm}while(++i<_n);}  // i runs from 0 to n-1, always at least once
#define DPUC(n,stm)      {I i=(n)+1;    do{stm}while(++i<0);}   // i runs from -n to -1 (faster than DO), always at least once
#define DQUC(n,stm)      {I i=-2-(I)(n);  do{stm}while(--i>=0);}  // i runs from n-1 downto 0, always at least once

/*
A: Our generic array container

From An APL Emulator:
    t  : type (0 is standard array, 1 is boxed array)
    r  : rank (0-3)
    d  : dimensions
    p  : data storage in two flavors, following An APL Emulator design.

For explanation of why p has two elements, see commentary on "from" operator and ga().
*/
typedef struct a {I t, r, d[3], p[2];} *A;

/*
   Unary and binary function application
*/
#define V1(f) A f(A w)
#define V2(f) A f(A a, A w)



#endif
